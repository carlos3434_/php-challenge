<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
use Exception;

class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}

	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		if(!isset($contact)) {
			throw new Exception("User was not found.");
		}
		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendSms($number, $message )
	{
		if( !isset($number) ) {
			throw new Exception(" You should send a phone number.");
		}
		if( !isset($message) ) {
			throw new Exception(" You should send a text message.");
		}
		$isValidNumber = ContactService::validateNumber($number);
		if (!$isValidNumber) {
			throw new Exception("You should send a valid phone number.");
		}
		return $this->provider->sendSms($number, $message);
	}

}
