<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;
use App\Call;
use App\Sms;
use App\Contact;
use App\Services\ContactService;
use App\Interfaces\CarrierInterface;
use Exception;
/**
 * @runTestsInSeparateProcesses
 */
class MobileTest extends TestCase
{
    protected $provider;

    protected function setUp(): void
    {
        parent::setUp();
        $this->provider = m::mock(CarrierInterface::class);
    }

    /** @test */
    public function it_returns_null_when_name_empty()
    {
        $mobile = new Mobile($this->provider);
        $this->assertNull($mobile->makeCallByName(''));
    }
    /** @test */
    public function make_a_call_when_user_was_found()
    {
        $name = 'Juan Perez';
        $contact = $this->settingProvider($name , '987654321' );

        m::mock('alias:'.ContactService::class)
            ->shouldReceive('findByName')
            ->withArgs(array($name) )
            ->andReturn($contact);
        $mobile = new Mobile($this->provider);
        $this->assertInstanceOf(Call::class, $mobile->makeCallByName($name));
    }
    /** @test */
    public function unable_to_make_a_call_when_user_was_not_found()
    {
        $name = 'Juan Perez';
        $call = m::mock('overload:'.Call::class);

        m::mock('alias:'.ContactService::class)
            ->shouldReceive('findByName')
            ->withArgs(array($name) )
            ->andReturn(null);
        $this->expectException(Exception::class);
        $mobile = new Mobile($this->provider);
        $mobile->makeCallByName($name);
    }

    /** @test */
    public function send_a_message_to_the_phone_number()
    {
        $sms = m::mock('overload:'.SMS::class);
        $message = 'test message';
        $phone = '987654321';
        $this->provider->shouldReceive('sendSms')
            ->withArgs([$phone, $message])
            ->andReturn($sms);

        m::mock('alias:'.ContactService::class)
            ->shouldReceive("validateNumber")
            ->withArgs([$phone])
            ->andReturn(true);

        $mobile = new Mobile($this->provider);

        $this->assertInstanceOf(SMS::class, $mobile->sendSms($phone, $message));
    }

    /** @test */
    public function unable_to_send_a_message_when_the_phone_is_invalid()
    {
        $sms = m::mock('overload:'.SMS::class);
        $message = 'test message';
        $phone = 'xyz';
        m::mock('alias:'.ContactService::class)
            ->shouldReceive('validateNumber')
            ->withArgs(array($phone))
            ->andReturn(false);

        $this->expectException(Exception::class);

        $mobile = new Mobile($this->provider);
        $mobile->sendSMS($phone, $message);
    }

    private function settingProvider($name , $phone )
    {
        $call = m::mock('overload:'.Call::class);
        $this->provider->shouldReceive('makeCall')
            ->andReturn($call);
        $contact = m::mock('overload:'.Contact::class);
        $contact->name = $name;
        $contact->phone = $phone;
        $this->provider->shouldReceive('dialContact')
            ->withArgs([$contact]);
        return $contact;
    }


}
